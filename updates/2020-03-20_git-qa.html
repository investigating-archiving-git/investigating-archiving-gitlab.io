<!--
.. title: Scholarly Git Experiences Part III: Quality Assurance 
.. slug: git-for-qa
.. date: 2020-03-20
.. author: Sarah Nguyen
.. link: https://gitlab.com/investigating-archiving-git/investigating-archiving-git.gitlab.io/blob/main/updates/2020-03-20_git-qa.html
.. description: A summary of how researchers and academics can use Git, Git hosting platforms, and continuous integration to perform quality assurance throughout their scholarship.
.. type: text
-->

<!DOCTYPE html>
<html>
<body>

    <p>It has been a few months since I last posted, but the open research movement has not stopped even in today's pandemic. Let's start this post on a light note with yesterday's quarantine experience:</p>

<p><figure class="center"><img src="/images/dogaYoga.png" alt=""><figcaption>Screenshot of a live Zoom yoga session during the COVID19 pandemic. Yoga teacher's dog is sliding down their back during downward dog position. Wishing you all peace, rest, and good health within your homes. Source: M. Nguyen</figcaption></figure></p>

<p>Now onto the good stuff. To date, I have discussed other features of these platforms, such as community building, education, and method tracking <a href="https://investigating-archiving-git.gitlab.io/updates/git-hosting-platforms-in-scholarship/">Part I</a> and <a href="https://investigating-archiving-git.gitlab.io/updates/git-for-education/">Part II</a>. This post continues upon that work and focuses on quality assurance.</p>

    <!--TEASER_END-->
    
    <p>TL;DR previous posts, check out the chart below. Green rows highlight the items this post will focus on:</p>

    <div class="table-responsive">
    <table class="table table-bordered table-hover">
    <thead class="thead-dark">
        <tr>
            <th>Scholarly Experience Topics</th>
            <th>Summary of Scholarly Experience Within a GHP</th>
            <th>Related GHP Feature</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">Version Control</th>
            <td>history of changes that allows scholars to gather a comprehensive understanding of how computations, scripts, and analyses have evolved through a study by viewing line-by-line changes in the codebase, as well as who in the research team they could contact for questions.</td>
            <td><ul><li>Commit logs</li><li>Branches</li></ul></td>
        </tr>
        <tr>
            <th scope="row">Community & collaboration</th>
            <td>When researchers publish data, scripts, and software used in their study onto a GHP, there are lower barriers for reuse "and [it can] serve as a powerful catalyst to accelerate progress" (Ram, 2013). When readers and researchers have easy access to a resource, such as "sequence and metadata from microbiome projects", there's less of a chance to receive "missing, incomplete, inconsistent and/or incomprehensible sequence and metadata, and reluctance by authors, editors, and publishers to react to our complaints" (Langille, 2018).</td>
            <td><ul><li>Issue tracker</li><li>Pull requests</li></ul></td>
        </tr>
        <tr>
            <th scope="row">Method tracking</th>
            <td>Researchers are writing protocols to aggregate, manage, and analyze data into lab and field notebooks. Git commit history gives researchers and team members reference to understand the iterative progress of a study. Data analysis pipelines, also known as processing workflows, can benefit from version control systems for exposure, collaborative feedback, and reproducibility of the methodology.</td>
            <td><ul><li>README</li><li>Wiki</li><li>Posts</li><li>Commit logs</li></ul></td>
        </tr>
        <tr>
            <th scope="row">Education</th>
            <td>Syllabi, OER, and course management can be hosted on GHPs. As seen in scholarly research, students and teachers also benefit from a GHP as the platform enhances collaboration, communication, feedback, provenance of course materials and submitted work, and exposes students to industry experience.</td>
            <td><ul><li>README</li><li>Wiki</li><li>CI</li><li>Issue tracker</li><li>Pull requests</li></ul></td>
        </tr>
        <tr class="table-success">
        <th scope="row">Quality assurance</th>
            <td>Merging small changes to the code often, throughout the development cycle. This tests bugs in data structure and code changes to ensure quality experimentation and analyses.</td>
            <td><ul><li>CI builds</li><li>YAML file</li></ul></td>
        </tr>
        <tr>
        <th scope="row">Reproducibility</th>
            <td colspan="2">NEXT</td>
        </tr>
        <tr>
            <th scope="row">Peer production</th>
            <td colspan="2">NEXT</td>
        </tr>

        <tr>
            <th scope="row">Publishing</th>
            <td colspan="2">NEXT</td>
        </tr>
    </tbody>
    </table>
    </div>
    <br/><br/>

    <h3>QA in Scholarship</h3>
        <p>QA == quality assurance. Commonly confused with quality control (QC), QA is a defined workflow that reveals whether or not "development and/or maintenance processes are adequate" for a system to meet standards and fulfill its purpose (<a href="https://www.acs.org/content/acs/en/careers/college-to-career/chemistry-careers/quality-assurance.html">The American Chemical Society</a>). Meanwhile, QC is a process done after the final product is created in order to weed out or flag the unexpected. Various academic and commercial fields maintain standards that help ensure the systems building a product (from mechanical parts to software) meet a basic level of quality in terms of function, safety, style, format, accessibility, etc. For example, remember <i>I Love Lucy</i> and the chocolate factory line? The conveyor belt, machinery, cooking, and shaping the chocolate would need to undergo QA by a technical manager watching over the processes. One might argue that Lucy and Ethel eating the chocolates could be QC, but that's more of an unrelated operational issue that I won't address here.</p>
        
        <p><figure class="center"><img src="/images/lucyChocoFactory_QA.png" alt=""><figcaption>Screenshot of <i>I Love Lucy</i> episode 39 "Job Switching" (1952). The chocolate factory manager conducts QA on the chocolate production process with Lucy and Ethel tasked at one part of the factory line. Source: <a href="https://famousclowns.org/lucille-ball/i-love-lucy/job-switching/">Famous Clowns</a>.</figcaption></figure></p>
        
        <p>We can look at  web archiving workflows, an example that's close to home for team IASGE. Here, QA includes inspecting a captured webpage to ensure that all elements on the archived web page function properly and that URLs redirect to the correct pages. This allows researchers and other web archive visitors to have a smooth experience when looking into how geocities might have behaved or how HTML 1.0 would have rendered.</p>
        
        <p>In the context of scholarly software development, QA steps are also taken before QC, but both the stages are necessary to ensure that features, functionality, and user experience of the software or script perform as expected with each new code release. QA takes place throughout the development process; it is like the flu shot, or practicing a consistent & healthy diet—preventing bugs from invading the system. The QA process flags low quality outputs and analyses as well as  code errors, allowing developers to address and fix bugs before sharing with collaborators and fellow researchers (Widder, et al., 2019; Yenni, et al., 2018). </p>

    <h3>Continuous Integration (CI)</h3>
    
        <p>CI is a practice or workflow included into software development workflows. CI platforms that facilitate the workflow are like robots that integrate with developing environments and "tell you, every time you change your code, if it still works or if you broke it" (Nowogrodzki, 2019), which can be likened to QA processes. Each integration of code can then be verified by automated builds and tests written by the developers. CI services can come from an external third-party system, but <a href="https://github.com/features/actions">GitHub Actions</a> and <a href="https://docs.gitlab.com/ce/ci/">GitLab CI</a> are popular services native to their respective GHP. Other popular CI platform options that integrate with GHPs and have been cited in scholarly research are <a href="https://travis-ci.com/">Travis CI</a>, <a href="https://circleci.com/">CircleCI</a>, and <a href="https://jenkins.io/">Jenkins</a> (Bendetto, et al., 2019). See <a href="https://ligurio.github.io/awesome-ci/">awesome-ci</a> for a more robust list of CI services. Many of these are proprietary platforms, but similar to GHPs, they rely on the open source community as their gateway user base to build into their premium subscription business model. Travis CI is the most popular CI service and has even been touted by the Zoteroist as the <a href="https://zoteromusings.wordpress.com/2014/09/25/quality-control-for-csl-styles/">automated first wall of defense</a>.</p>
        
        <p>To begin with CI, you must write a <a href="https://yaml.org/">YAML</a> file (specific to your CI service) in the Git repository that outlines the dependencies, tests, and workflows for running your analysis or data processing scripts (e.g. what comes first—preferably, the dependencies!). When new changes are pushed to that repository, the CI service reads the YAML file to build the test environment and run the steps as you specify them. While each YAML files' structure will differ depending on specific workflows and needs, here's a breakdown of how the <a href="https://gitlab.com/investigating-archiving-git/investigating-archiving-git.gitlab.io/-/blob/master/.gitlab-ci.yml">IASGE YAML file</a> that builds our website with GitLab CI:</p>
                
        <pre><code>image: registry.gitlab.com/paddy-hack/nikola:8.0.1</code></pre>
        
        <p>This first line in the YAML file identifies which Docker container we want to use from the GitLab Docker registry, which ensures we are starting with all the computational dependencies we need. 
        </p>
        
        <pre><code>test:
 script:
 - nikola build
 except:
 - master</code></pre>

        <p><code>test:</code> initiates the building of the test environment so that the newly added code can be tested before going to a production stage.</p>
       
       <pre><code>pages:
  script:
  - pip3 install pybtex
  - nikola plugin -i publication_list
  - nikola build
  artifacts:
  paths:
  - public
  only:
  - master</code></pre>
        
        <p><code>pages:</code> installs the extra dependencies that we need (the <code>publication_list</code> plugin and its dependency, <code>pybtex</code>) and then tries to build the website with the relevant Nikola commands. If it succeeds, it will publish the new changes to the website. If it fails, we are notified and the changes do not proceed.</p>

    <h3>CI for Scholarly Code</h3>
    
        <p>When developing and using software, scripts, or code for experiments or analyses, scholars can integrate QA into their research processes through "the practice of merging in small code changes frequently—rather than merging in a large change at the end of a development cycle" (<a href="https://docs.travis-ci.com/user/for-beginners/">Travis CI documentation, n.d.</a>). This practice is also known as continuous integration (CI). As research is becoming reliant on (a) correctly structured and processed data, and (b) increasingly homegrown software, pursuing QA for both (data and software) through CI is one way to automate both—checking that data is well-structured and that the code works the way it is expected to after those many small merges.</p>
    
        <p>Since CI is commonly integrated with code repositories (and in this case git repos), this allows the CI configuration files (used to build environments, tests, and pipelines) to be edited and used by multiple people,  while performing QA on snippets of code on a remote server. Bugs are flagged before versions are merged and integrated into the <code>master</code> codebase, helping researchers catch these early and often (<a href="https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/">GitLab Continuous Integration (CI) & Continuous Delivery (CD)</a>). A well-documented workflow starting from scratch is an excellent approach for improving reproducibility and replicability efforts (to be detailed in my next post).</p>
        
        <p><figure class="center"><img src="/images/CI.jpg" alt="2 lifecycle graphics, CI and continuous analysis, compared side-by-side. both have mirroring cyclical patterns: Plan & Design to code & build to push to version control system & test to release & deploy/re-run analyses to operate & monitor/analyze & draw conclusions"><figcaption>Similarities between CI and continuous analysis lifecycle that researchers can bake into their research and writing practices. Source: <a href="https://elifesciences.org/labs/e623676c/reproducibility-automated">Beaulieu-Jones (2017)</a>.</figcaption></figure></p>
        
        <p>However, CI for QA is useless without robust tests in place. While software engineering relies on testing processes to verify consistency within their QA checkpoints, this mirrors the term "scientific testing" redefined by Krafczyk, et al. (2019). In this case, scientific testing benefits from black-box (aka system) testing for being able to reproduce the computational environments and results being published in articles, as opposed to white-box (aka unit) testing. White-box testing is a QA practice that looks to verify only "specific functions or sections of code are [performing] properly". By contrast, black-box strives "to test [the behavior of] the whole application" (Krafczyk, et al., 2019), also referred to as specification-based testing. The tester (CI, in this case!) is aware of what the software is supposed to do but is not aware of how it does it. Here's one example: say that your research relies on merging together lots of different spreadsheets. You know that you are expecting one ID column from each spreadsheet, which is a number between 0000-1000. In the CI configuration file, you can write a test such that if there is any ID number outside that range, or if any values in that column are not numbers, it fails.</p>
        
        <p>Krafczyk, et al. (2019) sought to "construct testing scripts capable of running" whole computational procedures based on existing public scholarly repositories on GitHub, in which they found testing "minimized [versions of] computational experiments" are less time consuming and can accurately "mimic the original work... at a smaller scale" (Krafczyk, et al., 2019). Benefits from this minimized version testing approach include flagging software issues when changes are made, as well as identifying if "the results are sensitive to the computation environment" (Krafczyk, et al., 2019).</p>
        
        <p><a href="https://amueller.github.io/">Andreas Mueller</a>, Associate Research Scientist at Columbia University's Data Science Institute, discusses that with CI, researchers writing and running code to perform experiments or analyze data can be tested by "each individual component of the software to ensure that it is sound." In implementing CI, researchers and developers are able to:</p>
        
        <ol>
            <li>streamline and avoid tedious, manual work that is susceptible to errors</li>
            <li>make incremental code changes, lowering the risk of significant bugs or breaks</li>
            <li>receive fast review and feedback on code updates, allowing researchers to address bug fixes immediately and understand next steps needed for procedures and data collecting.</li>
        </ol>
        
        <p><figure class="center"><img src="/images/travisCI_postman.png" alt="Workflow graphic with images from terminal to push code to GHP to build on Travis CI to run Postman tests"><figcaption>Workflow from terminal to GHP to CI service to QA test results. Source: <a href="https://blog.postman.com/2017/08/23/integrate-api-tests-with-postman-newman-and-travis-ci/">Postman Blog</a></figcaption></figure></p>
        
        <p>For example, the Fermi National Accelerator Laboratory has written about their own CI service setup, where lab members collaborating with data collection and analytical code. It was clear (1) that the data needed to be properly structured, and (2) code updates were compatible through processing the data for analyses such as "comprehensive testing from unit tests to physics validation through regression test on multiple platforms/compiler/optimization that are supported by code developers" (Bendetto, et al., 2019). They are able to implement tests on code pull requests by implementing their own CI service with a Jenkins set up, which executes these phases sequentially: "environment setup, code checkout, build, unit tests, install, regression tests and validation tests" (Bendetto, et al., 2019). With the CI validation tests, researchers are able to monitor "the working directory; initialization and finalization procedures; the command to execute the task; logs to report; how to filter logs sent by the final report" (Bendetto, et al., 2019).</p>

<p>Another example comes from the University of Florida's Department of Wildlife Ecology and Conservation who set up "R code to error-check new data and update tables" (Yenni, et al., 2018) through Travis-CI implemented on their GitHub repo. CI will push notifications if changes to the code breaks the workflow, allowing researchers to immediately address the issue(s). It's like having a collaborator manually test the code experiment, and then send you an issue request with an error message if they were unable to replicate your code to expected results.</p>
        
        <p><figure class="center"><img src="/images/2018_CIWorkflow_Yenni_DevModernDataWorkflowEvolvingData.png" alt="Workflow graphic from data entry to manual QC to automated QA/QC to supplementary tables to version and archive"><figcaption>Figure 1: Data workflow from Yenni, et al. (2018). An illustrated workflow and the relevant systems used in each stage from data entry to manual QC to automated QA/QC to Supplementary tables to Version and archive.</figcaption></figure></p>
        
        <p>In short, this is the CI process within research:</p>
        
        <ol>
            <li>Researcher builds software or script and pushes it to update the Git repository.</li>
            <li>CI program clones the Git repo into a new and separate cloud-based virtual environment, usually defined by a Docker container and additional dependency installations.</li>
            <li>Code builds, runs, and tests in that new virtual environment through a series of tasks defined in a configuration file.</li>
            <li>CI will detect failed and successful tasks, and will send notification if the code is broken or passed, respectfully (<a href="https://docs.travis-ci.com/user/for-beginners/">Travis CI documentation</a>, n.d.).</li>
        </ol>

        <p>Et voilà, code can be updated to ensure its quality is up to standards and researchers can spend more time on innovation!</p>

        <p><figure class="center"><img src="/images/githubActions-pass.png" alt="Checkmark green message from GitHub when a Travis CI build successfully passes:'This pull request can be automatically merged.'"><figcaption>GitHub Actions’ notification when a code has been updated, submitted as a pull request, and successfully tested through CI services. Ready to be merged to the Master repo. Source: <a href="https://github.githubassets.com/images/modules/site/features/actions-pr-checks-final.svg">Features: GitHub Actions</a>.</figcaption></figure></p>

	<h3>Issues with CI in Scholarship</h3>
        <p>And last, nothing is perfect in technology nor life, so here are some of the cons to CI with GHPs for scholars and researchers. Widder, et al. (2019) does a great job in providing a comprehensive review of the known issues in adopting and/or using CI workflows and services within software development teams. From a full literature review, Widder, et al. (2019) categorizes five major pain points in CI within software developing workflows: information overload, organizational culture, setup and configuration, slow feedback, and testing deficiencies. Luckily, Krafczyk, et al. (2019) has proven examples in how to address CI’s slow feedback challenges through minimized version testing, as mentioned in the beginning of this post. Through mixed methods research (e.g. survey for quantitative and interviews for qualitative), Widder, et al. (2019) published a thorough table (<i>4: Results Summary</i>) of their findings in what pain points of CI could increase or decrease the likelihood of adopting the workflow and/or switching to incorporate CI. A few categories include: lack of support for specific languages from certain CI services, infrequent changes to the code itself means less of a need to constantly push tests and validation, and inconsistent CI practices among team members makes for a messier codebase. But, don’t let this deter you from trying out CI to help automate some QA workflows for your team! Giving it a try can yield really helpful, time-saving results, as outlined above.</p>

    <h3>Conclusion</h3>
        <p>In the end, CI can enable researchers to make progress with experiments, uninterrupted, unless an issue pops up from a CI run. While it's built for coders, scholars and researchers can benefit from the use of CI to automate (and validate!) parts of folks' research workflows. It is an interesting application of GHPs within a research context, and we look forward to seeing more CI in research repos as we move forward in our IASGE research.</p>

        <p>As mentioned in previous posts, these IASGE updates are active conversations while I continue my research about the scholarly Git experience. We encourage questions and further research suggestions. Please feel free to submit an issue or merge request to our <a href="https://gitlab.com/investigating-archiving-git/investigating-archiving-git.gitlab.io">GitLab repository</a> or email <a href="mailto:vicky.steeves@nyu.edu">Vicky Steeves</a> and/or <a href="mailto:sarahtnguyen@nyu.edu">me</a> to continue the conversation. Thanks for tuning in!</p>

    <h3>Bibliography</h3>
        
        <p>Akhmerov, A., Cruz, M., Drost, N., Hof, C., Knapen, T., Kuzak, M., Martinez-Ortiz, C., Turkyilmaz-van der Velden, Y., & van Werkhoven, B. (2019). Raising the Profile of Research Software. Zenodo. <a href="https://doi.org/10.5281/zenodo.3378572">https://doi.org/10.5281/zenodo.3378572</a></p>
        
        <p>Alexander, H., Johnson, L. K., & Brown, T. (2018). Keeping it light: (re)analyzing community-wide datasets without major infrastructure. GigaScience, 8(2). <a href="https://doi.org/10.1093/gigascience/giy159">https://doi.org/10.1093/gigascience/giy159</a></p>
                
        <p>Bai, X., Pei, D., Li, M., & Li, S. (2018). The DevOps Lab Platform for Managing Diversified Projects in Educating Agile Software Engineering. 2018 IEEE Frontiers in Education Conference (FIE), 1–5. <a href="https://doi.org/10.1109/FIE.2018.8658817">https://doi.org/10.1109/FIE.2018.8658817</a></p>
        
        <p>Beaulieu-Jones, B. K., & Greene, C. S. (2017). Reproducibility of computational workflows is automated using continuous analysis. Nature Biotechnology, 35(4), 342–346. <a href="https://doi.org/10.1038/nbt.3780">https://doi.org/10.1038/nbt.3780</a></p>

        <p>Benedetto, V. D., Podstavkov, V., Fattoruso, M., & Coimbra, B. (2019). Continuous Integration service at Fermilab. EPJ Web of Conferences, 214, 05009. <a href="https://doi.org/10.1051/epjconf/201921405009">https://doi.org/10.1051/epjconf/201921405009</a></p>

        <p>Krafczyk, M., Shi, A., Bhaskar, A., Marinov, D., & Stodden, V. (2019, June 1). Scientific Tests and Continuous Integration Strategies to Enhance Reproducibility in the Scientific Software Context. 2nd International Workshop on Practical Reproducible Evaluation of Computer Systems (P-RECS’19). ACM Federated Computing Research Conference, Phoenix, Arizona. <a href="https://doi.org/10.1145/3322790.3330595">https://doi.org/10.1145/3322790.3330595</a></p>
        
        <p>Stolberg, S. (2009). Enabling Agile Testing through Continuous Integration. 2009 Agile Conference, 369–374. <a href="https://doi.org/10.1109/AGILE.2009.16">https://doi.org/10.1109/AGILE.2009.16</a></p>
        
        <p>Widder, D. G., Hilton, M., Kästner, C., & Vasilescu, B. (2019). A conceptual replication of continuous integration pain points in the context of Travis CI. Proceedings of the 2019 27th ACM Joint Meeting on European Software Engineering Conference and Symposium on the Foundations of Software Engineering, 647–658. <a href="https://doi.org/10.1145/3338906.3338922">https://doi.org/10.1145/3338906.3338922</a></p>
        
        <p>Yenni, G. M., Christensen, E. M., Bledsoe, E. K., Supp, S. R., Diaz, R. M., White, E. P., & Ernest, S. K. M. (2018). Developing a modern data workflow for evolving data. BioRxiv, 344804. <a href="https://doi.org/10.1101/344804">https://doi.org/10.1101/344804</a></p>

</html>
</body>
