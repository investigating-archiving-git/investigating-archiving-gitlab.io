.. title: Our Scholarly Work
.. slug: scholarly-work
.. date: 2019-10-14
.. description: Scholarly output from IASGE.
.. type: text

.. publication_list:: bibtex/scholarship.bib
   :style: acm
   :detail_page_dir: 
